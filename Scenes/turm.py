from AdventureEngine.object import Object

__author__ = "Marvin Gerlach"

from AdventureEngine.scene import Scene


class Turm(Scene):
    def __init__(self):

        entrance = """Durch ein Fenster strahlt die kühle Morgensonne auf dein Bett unter dem du deinen wertvollsten Besitz versteckst.
 Neben dem Fenster befindet sich ein Schreibtisch. 
 Zu dem Zimmer führt eine einzige große schwere Tür.

 (Tippe "hilfe" für eine Liste aller Kommandos)
"""

        objects = [Object("Bett", False, False, command_answers=
        [
            ["benutze", ["Bett"], ["Genug geschlafen!"]],
            ["betrachte", ["Bett"], ["Mein altes Bett. Es hat einen Strohkern. Darunter verstecke ich meinen Koffer."]],
        ]),
                   Object("Schreibtisch", False, False, command_answers=
        [
            ["betrachte", ["Schreibtisch"],
             ["Auf dem Tisch liegen ein Brieföffner und Briefe von Antonia. Ich hoffe, ich werde sie bald wiedersehen."]],
        ]),
                   Object("Brieföffner", True, False, command_answers=
        [
            ["betrachte", ["Brieföffner"], ["Eine stumpfe Klinge zum Briefeöffnen"]]
        ]),
                   Object("Koffer", False, False, status="closed", command_answers=
        [
            ["benutze", ["Koffer"], ["Er ist zu."]],
            ["betrachte", ["Koffer"], ["Ein riesiger alter Lederkoffer"]],
            ["nimm", ["Koffer"], ["Auch wenn es ein Reisekoffer ist, ist er viel zu schwer um mitzunehmen."]]
            # TODO: Füge ein teleskop zur szene hinzu + Fenster interaktion
        ]),
                   Object("Tür", False, False, command_answers=
        [
            ["betrachte", ["Tür"], ["Eine große Holztür"]],
            ["öffne", ["Tür"], ["Sie ist verschlossen"]]
        ]),
                   Object("Briefe", True, False, command_answers=
        [
            ["benutze mit", ["Brieföffner", "Briefe"], ["Sie sind bereits offen"]],
            ["benutze mit", ["Briefe", "Brieföffner"], ["Sie sind bereits offen"]],
            ["betrachte", ["Briefe"], ["""Sie sind an mich adressiert:
     'Liebe Becky,
     ich habe seit Tagen nichts mehr von dir gehört. Bist du wohlauf?
     Ich mache mir Sorgen um dich, das kann so nicht mehr weitergehen.
     Lass uns zusammen von hier verschwinden. Nur du und ich.
     Wir lassen einfach unser altes Leben hinter uns und gehen nach Paris.
     Was hälst du davon?
     Bitte schreib alsbald zurück.

     Alles liebe,
     deine Antonia'

 Hach... Wenn es doch nur so einfach wäre..."""]],
        ]),
                   Object("Fenster", False, False, status="closed", command_answers=
        [
            ["betrachte", ["Fenster"], ["Vor dem Fenster befindet sich ein breiter Absatz, "
                                        "der über den Giebel zu den Fenstern im zweiten Stock führt."]],
            ["öffne", ["Fenster"], ["Es klemmt, aber es ist ein kleiner Spalt offen."]],
            ["benutze mit", ["Teleskop", "Fenster"], ["In der Stadt laufen viele uniformierte preußische Soldaten "
                                                      "herum. Ich sollte auf mich aufpassen.",
                                                      "Der Nachbarsjunge hat wieder mal keine Hose an. So ein Schelm... "
                                                      "Aber ein hübscher Schelm."]]
            # TODO: ändere status und lasse benutzen
        ])]

        Scene.__init__(self, "Turm", objects, entrance)


    def scene_magic(self, game_engine, inpt):
        a = inpt[0]
        o1 = self.find_object(inpt[1][0], game_engine.bag)
        o2 = ""
        if len(inpt[1]) == 2:
            o2 = self.find_object(inpt[1][1], game_engine.bag)
            if o1 and o2 and (not o1.in_bag and not o2.in_bag):
                game_engine.print("Ich muss eines dieser Dinge erst an mich bringen.")
                return

        if not o1:
            # No target object found
            game_engine.print_failure(a)
            return

        # nimm Sachen
        if a == "nimm" and o1.takeable and not o1.in_bag:
            o1.in_bag = True
            game_engine.bag.append(o1)
            self.objects.remove(o1)
            game_engine.print_success()
            return

        # öffne Koffer und füge Teleskop hinzu
        if a == "öffne" and o1.name == "Koffer" and o1.status == "closed":
            case = self.find_object("Koffer")
            case.status = "open"
            case.command_answers.append(["öffne", ["Koffer"], ["Er ist bereits offen."]])
            self.objects.append(Object("Teleskop", True, False, command_answers=
                                       [["betrachte", ["Teleskop"], ["Viele Nächte habe ich damit verbracht in die "
                                                                     "Sterne zu schauen, Kometen zu beobachten und "
                                                                     "herab zur Stadt zu blicken. Der Kosmos fand ich "
                                                                     "immer anspechender. Ich wünsche mir eines Tages "
                                                                     "mehr von ihm sehen zu können."]]]))
            game_engine.print("Darin ist mein Teleskop")
            return

        # öffne fenster mit brieföffner
        if o2 and a == "benutze mit" and self.both(o1, o2, "Fenster", "Brieföffner") and \
                (o1.status == "closed" or o2.status == "closed") and game_engine.in_bag("Brieföffner"):
            win = self.find_object("Fenster")
            win.status = "open"
            win.command_answers.pop(0)
            win.command_answers.pop(0)
            win.command_answers.append(["betrachte", ["Fenster"], ["Es ist offen."]])
            win.command_answers.append(["öffne", ["Fenster"], ["Es ist bereits offen."]])
            win.command_answers.append(["schließe", ["Fenster"], ["Ich lasse es mal offen. Es war schwer genug es aufzubekommen."]])
            game_engine.print("Das Fenster ist nun offen, der Brieföffner ist mir jedoch aus der Hand gefallen.")
            game_engine.bag.remove(self.find_object("Brieföffner", game_engine.bag))
            return

        # Benutze Fenster zum entkommen
        if a == "benutze" and o1.name == "Fenster" and o1.status == "open":
            if game_engine.in_bag("Teleskop"):
                game_engine.change_scene("Giebel")
            else:
                game_engine.print("Ich sollte alles wichtige mitnehmen")
            return

        # go through all objects and determine default answers
        return Scene.scene_magic(self, game_engine, inpt)



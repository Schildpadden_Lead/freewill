__author__ = "Marvin Gerlach"

from AdventureEngine.scene import Scene


class Draussen(Scene):
    def __init__(self):

        entrance = """Der Aufzug landet mit lautem Getöse im Erdgeschoss. Als du deine Augen öffnest, siehst du vor dir
 die Haustür weit aufgerissen. Von oben hört man Türen knallen und Leute brüllen. Du richtest deinen Blick nach draußen.
 Du siehst wie Leute mit Fackeln durch den Schnee stapfen. Als du zu der Tür hingehst, siehst du ein großes Feuer und
 dort herum versammelt eine ganze Menschenschar. Sie blicken auf eine Tribüne, auf der ein dunkel-schwarzes Schafott
 thront. Neben der Guillotine steht ein Scharfrichter in schwarzem Gewand.
 
 Als du durch die Menge tritts, erkennst du die Kleidung deines Vaters an dem Mann, der unter dem Fallbeil liegt. Er
 hat seine Hände hinter dem Rücken verbunden und trägt einen Leinensack über den Kopf gestülpt. An dem Aufstieg zur
 Tribüne siehst du deine jungen Geschwister auf dem Boden sitzen. Du siehst wie Claire und Joss ihre Tränen aus
 den Augen wischen, als sie dich sehen. Pierre schaut dich erschrocken an. Neben ihnen steht ein preußischer Polizist.
 
 Claire: 'Wir dachten du seist...' 
 Pierre: 'tot!'
 Polizist: 'Wie? Ist das etwa Rebecka, von der ihr behauptet habt, Francois hätte sie erschlagen?'
 Claire: 'Nein, das ist...'
 
 
 'Mein Name ist Rebecka.' (A)     ODER     Ich heiße Antonia. Sie müssen mich verwechseln.' (B)
"""

        Scene.__init__(self, "Draussen", [], entrance)


    def scene_magic(self, game_engine, input):
        if input == "a":
            self.ending_a(game_engine)
            self.credits(game_engine)
        elif input == "b":
            self.ending_b(game_engine)
            self.credits(game_engine)

    def ending_a(self, game_engine):

        game_engine.print("""Polizist: 'Soso. Stoppt den Prozess!'
 Scharfrichter: 'Was ist los?'
 Polizist: 'Die vermisste Person ist hier!'
 Francois: 'Becky? Becky ist hier? Wo ist sie? Ich will sie sehen!'
 
 Der Scharfrichter nimmt deinem Vater den Sack vom Kopf. 
 
 Francois: 'Meine Becky. Danke, dass du gekommen bist. Ich habe dich nur beschützen wollen. Es tut mir leid. Alles tut 
 mit so leid...'
 Ein Schaulustiger: 'Du hast uns an diese Preußischen Bastarde verraten! Lasst das Fallbeil runter sage ich!'
 
 Ein Tumult entsteht und preußische Polizisten versuchen, die Stimmung zu beruhigen. Ein Schuss ist zu hören und 
 die Menge fängt an auf die Tribüne zu stürmen. Im Getümmel erkennst du, dass der Schafrichter am Boden liegt.
 Mehr Schüsse fallen und die Meute schreitet auf das Schafott zu. Ehe du dich von deinen Geschwistern trennen kannst,
 siehst du die Klinge des Fallbeils, wie sie den in Tränen gehüllten Kopf deines Vaters erreicht. Sein abgetrenntes
 Haupt fällt in einen Bastkorb, der vorher für diesen Zweck dort platziert wurde.
 
 Ehe sich die Stimmung entspannt, entschließt du dich mit deinen Geschwister in die Stadt zu fliehen.
 
 ENDE
 
        
""")

    def ending_b(self, game_engine):

        game_engine.print("""Polizist: 'Dann verschwinde hier! Hier findet ein Prozess statt.'
 
 Pierre lächelt dich mit einem dankenden Blick an und nickt wohlwollend in Richtung Stadt.
 
 Du entschließt dich auf sein Geheiß dich auf den Weg zu machen. Am Ende des Hügelpfades kommt dir ein Mädchen entgegen.
 Sie lächelt dich an. Es ist Antonia.
 
 Antonia: 'Becky, was ist passiert? Ich habe gehört, dein Vater soll die Stadt verraten und dich erschlagen haben.
           Ich bin heilfroh, dass du wohlauf bist. Erzähl mir bitte alles, lass uns aber erst von hier verschwinden.'
           
 Zusammen mit deiner besten Freundin Antonia machst du dich auf in die Stadt, um dein neues Leben zu beginnen.
 Dafür musstest du nichts weiteres tun, als dein altes hinter dich zu lassen.

 ENDE


""")

    def credits(self, game_engine):
        game_engine.print("[enter]")
        game_engine.input()

        game_engine.print("""
        
        
        CREDITS
        
 Story and Programming by:
 Marvin Gerlach


 Special Thanks to:
 Benedikt Dennig & Michelle Benzel
 
 
 Thanks for Playing!
 
 

        """)

        game_engine.print("[enter]")
        game_engine.input()

        exit()





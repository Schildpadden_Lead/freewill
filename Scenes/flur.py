from AdventureEngine.object import Object

__author__ = "Marvin Gerlach"

from AdventureEngine.scene import Scene


class Flur(Scene):
    def __init__(self):

        entrance = """Du befindest dich im Hausflur des 2. Stocks. Vom Flur führt eine Treppe nach oben zu deinem 
 Zimmer und eine Treppe nach unten zum 1. Stock. Außerdem sind 3 Türen vorhanden: Die Tür zu Pierres Zimmer (Tür1),
 die Tür zur Küche (Tür2) und die Tür zum Arbeitszimmer deines Vaters (Tür3). Bis auf den Lärm von unten kannst
 du keine Geräusche ausmachen. An der Wand hängt ein Gemälde und auf dem Boden liegt ein langer persischer Teppich. 
"""

        objects = [Object("Treppe", False, False, command_answers=
        [
            ["betrachte", ["Treppe"], ["Die Treppe nach oben führt zu meinem Zimmer. "
                                       "Dorthin will ich nicht wieder zurück! Die Treppe nach unten führt zu "
                                       "den Schlafkammern von Joss und Claire und zum Erdgeschoss."]],
            ["benutze", ["Treppe"], ["Ich höre Schreie vom 1. Stockwerk. Was ist da los? "
                                     "Ich hoffe meinen Geschwistern geht es gut... "
                                     "\n Es muss einen anderen Weg hier raus geben."]]
        ]),
                   Object("Gemälde", False, False, command_answers=
        [
            ["betrachte", ["Gemälde"], ["Es zeigt meinen Vater mit geschwellter Brust posierend. Er sieht aus wie "
                                        "Ludwig XVI. Ich hoffe, ihn ereilt das gleiche Schicksal. Dieses Bild"
                                        " macht mich krank, ich wünschte ich könnte es zersäbeln."]]
        ]),
                   Object("Tür1", False, False, command_answers=
        [
            ["betrachte", ["Tür1"], ["Das ist Pierres Zimmer."]],
            ["öffne", ["Tür1"], ["Sie ist bereits offen."]]
        ]),
                   Object("Tür2", False, False, command_answers=
        [
            ["betrachte", ["Tür2"], ["Das ist die Küche."]],
            ["öffne", ["Tür2"], ["Sie ist bereits offen."]]
        ]),
                   Object("Tür3", False, False, status="closed", command_answers=
        [
            ["betrachte", ["Tür3"], ["Das ist Vaters Arbeitszimmer."]],
            ["öffne", ["Tür3"], ["Sie ist verschlossen."]],
            ["benutze", ["Tür3"], ["Sie ist verschlossen."]]
        ]),
                   Object("Teppich", False, False, command_answers=
        [
            ["betrachte", ["Teppich"], ["Ein großer Perserteppich, der fast den ganzen Boden bedeckt."]]
        ])]

        Scene.__init__(self, "Flur", objects, entrance)


    def scene_magic(self, game_engine, inpt):
        a = inpt[0]
        o1 = self.find_object(inpt[1][0], game_engine.bag)
        o2 = ""
        if len(inpt[1]) == 2:
            o2 = self.find_object(inpt[1][1], game_engine.bag)
            if o1 and o2 and (not o1.in_bag and not o2.in_bag):
                game_engine.print("Ich muss eines dieser Dinge erst an mich bringen.")
                return

        if not o1:
            # No target object found
            game_engine.print_failure(a)
            return

        # nimm Sachen
        if a == "nimm" and o1.takeable and not o1.in_bag:
            o1.in_bag = True
            game_engine.bag.append(o1)
            self.objects.remove(o1)
            game_engine.print_success()
            return

        # zu pierres zimmer
        if a == "benutze" and o1.name == "Tür1":
            game_engine.change_scene("Stube")
            return

        # zur küche
        if a == "benutze" and o1.name == "Tür2":
            game_engine.change_scene("Küche")
            return

        # zersäbel gemälde
        if o2 and a == "benutze mit" and self.both(o1, o2, "Messer", "Gemälde"):
            game_engine.print("Nimm das du Monster!\n \n Sowas... Hinter dem Gemälde ist ein kleiner Raum. "
                              "Das sieht aus wie ein Speiseaufzug. Eine ähnliche Luke habe ich am Eingang schon einmal gesehen.")
            self.objects.append(Object("Speiseaufzug", False, False, command_answers=
                                       [["betrachte", ["Speiseaufzug"], ["Ein kleiner Aufzug zum transportieren von Speisen."]]]))
            self.objects.remove(self.find_object("Gemälde"))
            return

        # benutze aufzug
        if a == "benutze" and o1.name == "Speiseaufzug":
            game_engine.print("Ich passe gerade so hinein.")
            game_engine.print("Sobald du in dem Aufzug Platz nimmst, löst sich ein Seil und du saust mit einem Ruck nach unten"
                              " ins Erdgeschoss,\n wo der Aufzug zum stehen kommt.")
            # raw input is important for final dialog
            game_engine.raw_input = True
            game_engine.change_scene("Draussen")
            return


        # go through all objects and determine default answers
        return Scene.scene_magic(self, game_engine, inpt)



from AdventureEngine.object import Object

__author__ = "Marvin Gerlach"

from AdventureEngine.scene import Scene


class Küche(Scene):
    def __init__(self):

        entrance = """Die Küche ist erst bei eurem Einzug in das Herrenhaus hinzugekommen. Hier befindet sich ein Herd,
 ein paar Schränke, ein Abfalleimer, zwei Stühle, ein Tresen, ein Fenster und eine Tür zum Flur.
"""

        objects = [Object("Herd", False, False, status="off", command_answers=
        [
            ["betrachte", ["Herd"], ["""Ein moderner Gasherd. Er ist allerdings kalt. Gestern wurde hier noch von unserem Hausmädchen für uns gekocht. 
 Vater behandelt sie besser als seine eigenen Kinder."""]],
            ["öffne", ["Herd"], ["Eine Gasflasche ist angeschlossen."""]],
            ["benutze", ["Herd"], ["Ich muss ihn erst anzünden."]]
        ]),
                   Object("Fenster", False, False, status="closed", command_answers=
        [
            ["betrachte", ["Fenster"], ["Der Giebel ist zu sehen."]],
            ["benutzen", ["Fenster"], ["Es ist geschlossen."]],
            ["öffne", ["Fenster"], ["Es klemmt."]],
        ]),
                   Object("Schränke", False, False, command_answers=
        [
            ["betrachte", ["Schränke"], ["Hier sind nur Teller, Töpfe und Pfannen. Nichts, was mir weiterhelfen kann."]]
        ]),
                   Object("Abfalleimer", False, False, command_answers=
        [
            ["betrachte", ["Abfalleimer"], ["Da sind noch die Reste unseres Abendessens drin. Igitt."]],
            ["nimm", ["Abfalleimer"], ["Auf gar keinen Fall lange ich da rein!"]]
        ]),
                   Object("Stühle", False, False, command_answers=
        [
            ["betrachte", ["Stühle"], ["Am Rand stehen zwei Holzstühle aus dunklem Ebenholz. Hinter einem "
                                       "der Stühle ist ja ein Mauseloch!"]],
            ["benutze", ["Stühle"], ["Ich will mich nicht setzen."]]
        ]),
                   Object("Tresen", False, False, command_answers=
        [
            ["betrachte", ["Tresen"], ["Auf dem Tresen steht ein großer Topf."]],
        ]),
                   Object("Mauseloch", False, False, command_answers=
        [
            ["betrachte", ["Mauseloch"], ["Das ist mir vorher noch nie aufgefallen. Ich frage mich,"
                                          " ob die Maus zuhause ist... Nein, leider nicht.\n Dafür liegt da aber eine "
                                          "Mausefalle drin!"]]
        ]),
                   Object("Mausefalle", True, False, command_answers=
        [
            ["betrachte", ["Mausefalle"], ["Der Bügel ist gespannt, doch der Speck ist weg. Sie schnellt zusammen, "
                                           "sobald etwas die spitze berührt."]],
            ["benutze", ["Mausefalle"], ["Autsch!"]]
        ]),
                   Object("Topf", True, False, status="solid", command_answers=
        [
            ["betrachte", ["Topf"], ["Er ist randvoll mit eingebranntem Frittierfett. \n Hey, da schaut ein Griff "
                                     "aus dem Fett heraus."]],
            ["öffne", ["Topf"], ["Das Fett ist zu hart, um einfach reinzugreifen."]]
        ]),
                   Object("Tür", False, False, command_answers=
        [
            ["betrachte", ["Tür"], ["Eine solide Küchentür."]],
            ["öffne", ["Tür"], ["Sie ist bereits offen."]],
            ["schließe", ["Tür"], ["Ich lasse sie besser offen."]]
        ]),
                   Object("Griff", False, False, command_answers=
        [
            ["betrachte", ["Griff"], ["Ein Dunkler Holzgriff. Was ist das für ein Ding?"]],
            ["nimm", ["Griff"], ["Er steckt zu tief in dem Fett, ich bekomme ihn kaum gegriffen."]],
            ["ziehe", ["Griff"], ["Meine Hände rutschen davon ab."]],
            ["drücke", ["Griff"], ["Na toll, jetzt ist er noch weiter drin..."]]
        ])]

        Scene.__init__(self, "Küche", objects, entrance)


    def scene_magic(self, game_engine, inpt):
        a = inpt[0]
        o1 = self.find_object(inpt[1][0], game_engine.bag)
        o2 = ""
        if len(inpt[1]) == 2:
            o2 = self.find_object(inpt[1][1], game_engine.bag)
            if o1 and o2 and (not o1.in_bag and not o2.in_bag):
                game_engine.print("Ich muss eines dieser Dinge erst an mich bringen.")
                return

        if not o1:
            # No target object found
            game_engine.print_failure(a)
            return

        # nimm Sachen
        if a == "nimm" and o1.takeable and not o1.in_bag:
            o1.in_bag = True
            game_engine.bag.append(o1)
            self.objects.remove(o1)
            game_engine.print_success()
            return

        # zünde herd an
        if o2 and a == "benutze mit" and self.both(o1, o2, "Herd", "Streichhölzer") and self.find_object("Herd").status == "off":
            game_engine.print("Die Flamme brennt. Der Herd ist nun an.")
            self.find_object("Herd").status = "on"
            self.find_object("Herd").command_answers.pop(2)
            self.find_object("Herd").command_answers.append(["benutze", ["Herd"], ["Dafür muss ich etwas draufstellen."]])
            return

        # stelle den topf auf den Herd
        if o2 and a == "benutze mit" and self.both(o1, o2, "Herd", "Topf") and self.find_object("Herd").status == "on":
            game_engine.print("Das Fett beginnt langsam zu schmelzen. Der Griff darin gehört zu einem Messer! ")
            topf = self.find_object("Topf", game_engine.bag)
            topf.status = "liquid"
            topf.takeable = False
            topf.command_answers = [["betrachte", ["Topf"], ["Das Messer schwimmt in dem kochend heißen Fett."]],
                                    ["nimm", ["Topf"], ["Die Griffe sind viel zu heiß, um ihn zu nehmen"]]]
            messer = Object("Messer", False, False, command_answers=
                            [["betrachte", ["Messer"], ["Es schwimmt in dem heißen Fett."]],
                             ["nimm", ["Messer"], ["Das würde mir die Finger verbrennen!"]]])
            self.objects.append(messer)
            self.objects.append(topf)
            self.objects.remove(self.find_object("Griff"))
            game_engine.bag.remove(topf)
            return

        # nimm mit der mausefalle das messer
        if o2 and self.find_object("Messer") and not self.find_object("Messer").in_bag and\
                        a == "benutze mit" and\
                (self.both(o1, o2, "Messer", "Mausefalle") or self.both(o1, o2, "Topf", "Mausefalle")):
            messer = self.find_object("Messer")
            messer.command_answers = [["betrachte", ["Messer"], ["Ein scharfes Küchenmesser. Meine roten Haare spiegeln sich"
                                                                 " in der Klinge."]]]
            messer.in_bag = True
            self.objects.remove(messer)
            game_engine.bag.append(messer)
            game_engine.bag.remove(self.find_object("Mausefalle", game_engine.bag))
            game_engine.print("Wenn ich die Falle an dem einen Ende festhalte und mit dem anderen Ende nach dem Messer"
                              " greife, könnte ich es herausnehmen...\n Es hat funktioniert! \n Die Falle brauche ich nicht mehr.")
            return

        # steige durch fenster
        if a == "benutze" and o1.name == "Fenster" and o1.status == "open":
            game_engine.change_scene("Giebel")
            return

        # benutze tür
        if a == "benutze" and o1.name == "Tür":
            game_engine.change_scene("Flur")
            return

        # go through all objects and determine default answers
        return Scene.scene_magic(self, game_engine, inpt)



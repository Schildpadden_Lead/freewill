from AdventureEngine.object import Object

__author__ = "Marvin Gerlach"

from AdventureEngine.scene import Scene


class Vorratsraum(Scene):
    def __init__(self):

        self.darkness = True

        entrance = """Du steigst eine steile Leiter hinab in eine dunkle Kammer.
 Durch die Dunkelheit erkennst du einen kleinen Raum.
 Das Licht aus Pierres Zimmer reicht allerdings nicht aus, um alles auszumachen. 
"""

        objects = [Object("Leiter", False, False, command_answers=
        [
            ["betrachte", ["Leiter"], ["Sie führt in Pierres Zimmer."]]
        ]),
                   Object("Dunkelheit", False, False, command_answers=
       [
           ["betrachte", ["Dunkelheit"], ["Eine wohlige Finsternis umgibt den Raum."]]
       ])]

        Scene.__init__(self, "Vorratsraum", objects, entrance)



    def scene_magic(self, game_engine, inpt):
        a = inpt[0]
        o1 = self.find_object(inpt[1][0], game_engine.bag)
        o2 = ""
        if len(inpt[1]) == 2:
            o2 = self.find_object(inpt[1][1], game_engine.bag)
            if o1 and o2 and (not o1.in_bag and not o2.in_bag):
                game_engine.print("Ich muss eines dieser Dinge erst an mich bringen.")
                return

        if not o1:
            # No target object found
            game_engine.print_failure(a)
            return

        # nimm Sachen
        if a == "nimm" and o1.takeable and not o1.in_bag:
            o1.in_bag = True
            game_engine.bag.append(o1)
            self.objects.remove(o1)
            game_engine.print_success()
            return

        # entzünde Kerze
        if o2 and a == "benutze mit" and self.both(o1, o2, "Kerze", "Streichhölzer") and self.darkness:
            game_engine.print("""Du entzündest die Kerze. Ein schwacher Lichtschein fällt auf einen alten Lagerraum 
 mit großen Fässern in einem Regal. Du stellst die Kerze in einen Kerzenständer auf dem Boden. 
 An der Wand hängen Zeichnungen. Auf Bauchhöhe erkennst du eine kleine Puppe in dem Regal sitzen.
""")
            game_engine.bag.remove(self.find_object("Kerze", game_engine.bag))

            self.objects = [Object("Leiter", False, False, command_answers=
            [
                ["betrachte", ["Leiter"], ["Sie führt in Pierres Zimmer."]]
            ]),
                           Object("Kerze", False, False, command_answers=
           [
               ["betrachte", ["Kerze"], ["Sie erhellt den Raum."]],
               ["nimm", ["Kerze"], ["Hier ist sie am besten aufgehoben."]]
           ]),
                           Object("Fässer", False, False, command_answers=
           [
               ["betrachte", ["Fässer"], ["Es sind große Weinfässer. Sie sehen sehr alt aus."]]
           ]),
                           Object("Puppe", False, False, command_answers=
           [
               ["betrachte", ["Puppe"], ["Eine selbstgemachte Puppe. Sie sieht aus wie Vater. An der Stelle"
                                         " wo ihr Herz sein sollte, steckt ein langer Spieß. "
                                         "Ein beruhigender und zugleich grausiger Anblick."]],
               ["nimm", ["Puppe"], ["Die kommt mir nicht in die Tasche!"]]
           ]),
                           Object("Spieß", True, False, command_answers=
           [
               ["betrachte", ["Spieß"], ["Ein langer Metallspieß um Rollbraten zusammenzuhalten."]]
           ]),
                           Object("Zeichnungen", False, False, command_answers=
           [
               ["betrachte", ["Zeichnungen"], ["Sie zeigen eine triumphierende, rothaarige Frau,"
                                               " die auf einem toten Riesen mit Schweinsnase steht,"
                                               " dem sie gerade ein Schwert in den Bauch gestoßen hat."]]
           ])]
            return

        if a == "benutze" and o1.name == "Leiter":
            game_engine.change_scene("Stube")
            return

        # go through all objects and determine default answers
        return Scene.scene_magic(self, game_engine, inpt)



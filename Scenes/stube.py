from AdventureEngine.object import Object

__author__ = "Marvin Gerlach"

from AdventureEngine.scene import Scene


class Stube(Scene):
    def __init__(self):

        entrance = """Pierre ist mit 12 Jahren das zweitälteste Kind des gewalttätigen Vaters. 
 Sein Zimmer beinhaltet einen kleinen Ofen, ein frisch gemachtes Bett, unter dem ein Teppich aus Damast liegt. 
 An der Wand hängen Zeichnungen, darunter liegen Malsachen. Zu dem Zimmer führt eine Tür, ähnlich zu der in
 Beckys Zimmer. Das Fenster steht offen.
"""

        objects = [Object("Ofen", False, False, command_answers=
        [
            ["öffne", ["Ofen"], ["Darin liegen Streichhölzer."]],
            ["betrachte", ["Ofen"], ["Ein alter gußeiserner Ofen mit einer Klappe um Holz einzulegen. "
                                     "Er hat einem Abzug nach draußen."]],
            ["benutze mit", ["Streichhölzer", "Ofen"], ["Es ist nichts zum anzünden darin."]],
            ["benutze mit", ["Ofen", "Streichhölzer"], ["Es ist nichts zum anzünden darin."]],
        ]),
                   Object("Bett", False, False, command_answers=
       [
           ["betrachte", ["Bett"], ["Ein sauber gemachtes Bett. Das sieht Pierre gar nicht ähnlich. Es sieht fast so aus,"
                                    " als hätte er gar nicht darin geschlafen. \n Ich ahne schreckliches."]]
       ]),
                   Object("Teppich", False, False, status="unaccessible", command_answers=
        [
            ["betrachte", ["Teppich"], ["Ein sehr schöner Teppich, auch wenn er Falten unter dem Bett wirft. "
                                        "Die quadratischen Abdrücke im Teppich stimmen nicht mit der Position"
                                        " der Bettstempel überein."]],
            ["ziehe", ["Teppich"], ["Das Bett steht darauf."]]
        ]),
                   Object("Tür", False, False, status="closed", command_answers=
        [
            ["betrachte", ["Tür"], ["Sie ist von außen verschlossen. "
                                    "Allerdings steckt der Schlüssel noch im Schlüsselloch. "
                                    "Unter der Tür strömt kalte Luft hindurch."]],
            ["benutze mit", ["Tür", "Briefe"], ["Die Idee ist gut, aber ich will das ihnen nicht antun. "
                                                "Es gibt hier sicher auch anderes Papier..."]]
        ]),
                   Object("Schlüsselloch", False, False, command_answers=
        [
            ["betrachte", ["Schlüsselloch"], ["Der Schlüssel steckt auf der anderen Seite der Tür."]],
            ["benutze mit", ["Stift", "Schlüsselloch"], ["Er ist zu kurz."]],
            ["benutze mit", ["Schlüsselloch", "Stift"], ["Er ist zu kurz."]],
            ["benutze mit", ["Streichhölzer", "Schlüsselloch"], ["Sie brechen ab, ehe ich mit ihnen genug Kraft aufwenden kann. "
                                                               "Es muss eine andere Möglichkeit geben."]],
            ["benutze mit", ["Schlüsselloch", "Streichhölzer"],
             ["Sie brechen ab, ehe ich mit ihnen genug Kraft aufwenden kann."
              "Es muss eine andere Mglichkeit geben."]],
            ["drücke", ["Schlüsselloch"], ["Meine Finger sind zu dick."]],
            ["nimm", ["Schlüsselloch"], ["Meine Finger sind zu dick."]]
        ]),
                   Object("Schlüssel", False, False, status="stuck", command_answers=
        [
            ["betrachte", ["Schlüssel"], ["Er steckt auf der anderen Seite der Tür."]],
            ["benutze mit", ["Stift", "Schlüssel"], ["Er ist zu kurz."]],
            ["benutze mit", ["Schlüssel", "Stift"], ["Er ist zu kurz."]],
            ["benutze mit", ["Streichhölzer", "Schlüssel"], ["Sie brechen ab, ehe ich mit ihnen genug Kraft aufwenden kann. "
                                                               "Es muss eine andere Möglichkeit geben."]],
            ["benutze mit", ["Schlüssel", "Streichhölzer"],
             ["Sie brechen ab, ehe ich mit ihnen genug Kraft aufwenden kann."
              "Es muss eine andere Möglichkeit geben."]],
            ["drücke", ["Schlüssel"], ["Meine Finger sind zu dick."]],
            ["nimm", ["Schlüssel"], ["Meine Finger sind zu dick."]]
        ]),
                   Object("Zeichnungen", False, False, command_answers=
       [
           ["betrachte", ["Zeichnungen"], ["Sie zeigen unser Haus mit großen schwarzen Punkten darunter."]],
           ["benutze mit", ["Stift", "Zeichnungen"],
            ["Ich mal noch eben eine Sonne in die Ecke. \n So, sieht schon gleich viel freundlicher aus."]],
           ["benutze mit", ["Zeichnungen", "Stift"],
            ["Ich mal noch eben eine Sonne in die Ecke. \n So, sieht schon gleich viel freundlicher aus."]],
       ]),
                   Object("Malsachen", False, False, command_answers=
       [
           ["betrachte", ["Malsachen"], ["Hier liegen ein Blatt Papier, eine Kerze und ein Stift auf dem Boden."]]
       ]),
                   Object("Stift", True, False, command_answers=
       [
           ["betrachte", ["Stift"], ["Ein zirka 2 cm langer Stumpen von einem Buntstift."]],
           ["benutze mit", ["Papier", "Stift"], ["Ich weiß nicht, was ich draufschreiben soll."]],
           ["benutze mit", ["Stift", "Papier"], ["Ich weiß nicht, was ich draufschreiben soll."]]
       ]),
                   Object("Fenster", False, False, command_answers=
       [
           ["öffne", ["Fenster"], ["Es ist bereits offen."]],
           ["betrachte", ["Fenster"], ["Draus vom Dache, da komm ich her..."]],
           ["schließe", ["Fenster"], ["Ich lasse es mal offen."]]
       ]),
                   Object("Streichhölzer", True, False, command_answers=
       [
           ["betrachte", ["Streichhölzer"], ["Die Schachtel ist randvoll."]],
           ["benutze", ["Streichhölzer"], ["Ich will nichts anzünden."]],
           ["benutze mit", ["Streichhölzer", "Kerze"], ["Es ist hell genug."]],
           ["benutze mit", ["Kerze", "Streichhölzer"], ["Es ist hell genug."]]
       ]),
                   Object("Kerze", True, False, command_answers=
       [
           ["betrachte", ["Kerze"], ["Eine halbe Wachskerze mit verrußtem Docht."]]
       ]),
                   Object("Papier", True, False, command_answers=
       [
           ["betrachte", ["Papier"], ["Ein weißes Blatt Papier."]]
       ])
                   ]

        Scene.__init__(self, "Stube", objects, entrance)


    def scene_magic(self, game_engine, inpt):
        a = inpt[0]
        o1 = self.find_object(inpt[1][0], game_engine.bag)
        o2 = ""
        if len(inpt[1]) == 2:
            o2 = self.find_object(inpt[1][1], game_engine.bag)
            if o1 and o2 and (not o1.in_bag and not o2.in_bag):
                game_engine.print("Ich muss eines dieser Dinge erst an mich bringen.")
                return

        if not o1:
            # No target object found
            game_engine.print_failure(a)
            return

        # nimm Sachen
        if a == "nimm" and o1.takeable and not o1.in_bag:
            o1.in_bag = True
            game_engine.bag.append(o1)
            self.objects.remove(o1)
            game_engine.print_success()
            # entferne Malsachen
            if self.find_object("Malsachen") and (o1.name == "Stift" or o1.name == "Papier" or o1.name == "Kerze"):
                self.objects.remove(self.find_object("Malsachen"))
            return

        # steige durch fenster
        if a == "benutze" and o1.name == "Fenster":
            game_engine.change_scene("Giebel")
            return

        # schiebe bett weg
        if (a == "drücke" or a == "ziehe") and o1.name == "Bett" and self.find_object("Teppich").status == "unaccessible":
            o1.command_answers.append(["schiebe", ["Bett"], ["Ich habe bereits den Teppich freigelegt."]])
            carp = self.find_object("Teppich")
            carp.status = "accessible"
            carp.command_answers.pop(1)
            game_engine.print("Puh, geschafft. Das Bett steht nun nicht mehr auf dem Teppich.")
            return

        # ziehe Teppich weg
        if a == "ziehe" and o1.name == "Teppich" and o1.status == "accessible":
            o1.command_answers = [["ziehe", ["Teppich"], ["Ich habe bereits die Luke freigelegt."]]]
            o1.status = "away"
            luke = Object("Luke", False, False, status="closed",
                          command_answers=[["betrachte", ["Luke"], ["Eine Luke im Boden! Sie führt in den ersten Stock."]],
                                           ["benutze", ["Luke"],["Sie ist zu."]]])
            self.objects.append(luke)
            game_engine.print("Unter dem Teppich ist eine Luke!")
            return

        # öffne Luke
        if a == "öffne" and o1.name == "Luke" and o1.status == "closed":
            o1.status = "open"
            o1.command_answers.pop(1)
            game_engine.print_success()
            return

        # steige Luke herab zum Vorratsraum
        if a == "benutze" and o1.name == "Luke" and o1.status == "open":
            game_engine.change_scene("Vorratsraum")
            return

        # Platziere papier unter der tür
        if o2 and (a == "benutze mit" or a == "gebe an") and self.both(o1, o2, "Papier", "Tür"):
            paper = self.find_object("Papier", game_engine.bag)
            paper.in_bag = False
            paper.takeable = False
            paper.command_answers.pop()
            paper.command_answers.append(["betrachte", ["Papier"], ["Es liegt unter der Tür"]])
            paper.status = "under door"
            self.objects.append(paper)
            game_engine.bag.remove(paper)
            game_engine.print("Es liegt nun in dem Spalt unter der Tür")
            return

        # Drücke Schlüssel mit Spieß heraus, wenn Papier unter Tür
        if o2 and a == "benutze mit" and (self.both(o1, o2, "Schlüsselloch", "Spieß")
                or self.both(o1, o2, "Schlüssel", "Spieß")) and self.find_object("Schlüssel").status == "stuck":
            if self.find_object("Papier").status == "under door":
                key = self.find_object("Schlüssel")
                key.status = "free"
                key.command_answers = [["betrachte", ["Schlüssel"], ["Der Schlüssel zu Pierres Zimmer"]]]
                key.in_bag = True
                game_engine.bag.append(key)
                self.objects.remove(key)
                self.find_object("Schlüsselloch").command_answers = []
                self.find_object("Tür").command_answers = [["betrachte", ["Tür"], ["Eine massive Holztür."]],
                                            ["öffne", ["Tür"], ["Ich sollte den Schlüssel erst benutzen."]]]
                game_engine.print("Der Schlüssel ist auf das Papier gefallen. Jetzt ziehe ich daran und... Ha! Hab dich!")
            else:
                game_engine.print("Ich will nicht, dass der Schlüssel einfach auf den Boden fällt. So komme ich nicht heran.")
            return

        # Öffne Tür mit Schlüssel
        if o2 and a == "benutze mit" and (self.both(o1, o2, "Schlüssel", "Tür") or self.both(o1, o2, "Schlüsselloch", "Tür")) \
                and self.find_object("Schlüssel", game_engine.bag).status == "free":
            door = self.find_object("Tür")
            door.command_answers = [["betrachte", ["Tür"], ["Eine massive Holztür. Der Schlüssel steckt."]],
                                    ["öffne", ["Tür"], ["Sie ist bereits offen."]]]
            door.status = "open"
            key = self.find_object("Schlüssel", game_engine.bag)
            key.status = "back"
            key.in_bag = False
            game_engine.bag.remove(key)
            self.objects.append(key)
            game_engine.print("Sie ist offen!")
            return

        # Benutze Tür
        if a == "benutze" and o1.name == "Tür" and o1.status == "open":
            game_engine.print("""Als du die Tür öffnest hörst du ein lautes Geräusch aus dem unteren Teil des Hauses.
 Stiefelschritte und Brüllen ertönt im ganzen Haus. 
            """)
            game_engine.change_scene("Flur")
            return

        # go through all objects and determine default answers
        return Scene.scene_magic(self, game_engine, inpt)



from AdventureEngine.object import Object

__author__ = "Marvin Gerlach"

from AdventureEngine.scene import Scene


class Giebel(Scene):
    def __init__(self):

        entrance = """Du trittst durch das Fenster auf den Absatz. Der eisige Wind lässt dich am ganzen Leib zittern.
 Vor dir liegen zwei Fenster (Fenster1 und Fenster2). Ein einem Ende ist dein Turmfenster.
 Am anderen Ende ist ein eiserner Wetterhahn.
"""

        objects = [Object("Wetterhahn", False, False, command_answers=
        [
            ["betrachte", ["Wetterhahn"], ["Er zeigt nach Nord-Osten, entgegen der Windrichtung."]],
            ["rede mit", ["Wetterhahn"], ["Putt Putt Putt!\n\n Er reagiert nicht."]],
            ["nimm", ["Wetterhahn"], ["Ich komme nicht an ihn heran. Er ist zu weit oben."]]
        ]),
                   Object("Turmfenster", False, False, command_answers=
        [
            ["betrachte", ["Turmfenster"], ["Das ist das Fenster zu meinem Zimmer."]],
            ["öffne", ["Turmfenster"], ["Es ist bereits offen."]]
        ]),
                   Object("Fenster1", False, False, status="closed", command_answers=
        [
            ["betrachte", ["Fenster1"], ["Das ist Pierres Zimmer. Er ist gerade nicht da. \n Komisch."]],
            ["benutze", ["Fenster1"], ["Es ist zu."]]
        ]),
                   Object("Fenster2", False, False, status="closed", command_answers=
        [
            ["betrachte", ["Fenster2"], ["Das ist die Küche."]],
            ["benutze", ["Fenster2"], ["Es ist zu."]],
            ["öffne", ["Fenster2"], ["Ich bekomme es nicht auf."]]
        ]),
                   Object("Wind", False, False, command_answers=
        [
            ["betrachte", ["Wind"], ["Der eisige Hauch der Freiheit."]]
        ])]

        Scene.__init__(self, "Giebel", objects, entrance)


    def scene_magic(self, game_engine, inpt):
        a = inpt[0]
        o1 = self.find_object(inpt[1][0], game_engine.bag)
        o2 = ""
        if len(inpt[1]) == 2:
            o2 = self.find_object(inpt[1][1], game_engine.bag)
            if o1 and o2 and (not o1.in_bag and not o2.in_bag):
                game_engine.print("Ich muss eines dieser Dinge erst an mich bringen.")
                return

        if not o1:
            # No target object found
            game_engine.print_failure(a)
            return

        # nimm Sachen
        if a == "nimm" and o1.takeable and not o1.in_bag:
            o1.in_bag = True
            game_engine.bag.append(o1)
            self.objects.remove(o1)
            game_engine.print_success()
            return

        # öffne fenster1
        if a == "öffne" and o1.name == "Fenster1" and o1.status == "closed":
            o1.status = "open"
            o1.command_answers.pop(1)
            o1.command_answers.append(["öffne", ["Fenster"], ["Es ist bereits offen."]])
            o1.command_answers.append(["schließe", ["Fenster"], ["Ich lasse es lieber offen."]])
            game_engine.print_success()
            return

        # steige durch fenster1
        if a == "benutze" and o1.name == "Fenster1" and o1.status == "open":
            game_engine.change_scene("Stube")
            return

        # steige durch fenster2
        if a == "benutze" and o1.name == "Fenster2" and o1.status == "open":
            game_engine.change_scene("Küche")
            return

        # steige durch turmfenster
        if a == "benutze" and o1.name == "Turmfenster":
            game_engine.change_scene("Turm")
            return


        # go through all objects and determine default answers
        return Scene.scene_magic(self, game_engine, inpt)



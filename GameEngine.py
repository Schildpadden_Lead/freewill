__author__ = "Marvin Gerlach"

"""

This class is the base class for a new text adventure.

A text adventure consists of multiple scenes with objects the player can interact with via some commands.
A command has the form of a string with object placeholders {0}, {1}, ...
E.g. "open {0} with {1}" or "go to {0}"
The command dic has the command name as key and the command string as value: {"open": "open {0}", ...}

"""

from numpy.random import choice
import pickle
from Scenes.turm import Turm
from Scenes.giebel import Giebel
from Scenes.stube import Stube
from Scenes.draussen import Draussen
from Scenes.flur import Flur
from Scenes.küche import Küche
from Scenes.vorratsraum import Vorratsraum
from colors import colors
import readline


class GameEngine():
    def __init__(self):
        self.command_dic = {
            "öffne": "öffne {0}",
            "schließe": "schließe {0}",
            "benutze": "benutze {0}",
            "benutze mit": "benutze {0} mit {1}",
            "rede mit": "rede mit {0}",
            "ziehe": "ziehe {0}",
            "drücke": "drücke {0}",
            "betrachte": "betrachte {0}",
            "gebe": "gebe {0} an {1}",
            "nimm": "nimm {0}"
        }

        # special commands are technic related and have no arguments
        self.special_commands = {
            "hilfe": "hilfe",
            "tasche": "tasche",
            "speichern": "speichern",
            "beenden": "beenden",
            "umsehen": "umsehen"
        }

        self.sccuess_phrases = [
            "Alles klar",
            "Okay",
            "Das kann ich machen",
            "Es hat funktioniert",
            "Gut",
            "Très Bien"
        ]

        self.failure_dic = {
            "öffne": ["Das lässt sich nicht öffnen."],
            "öffne mit": ["Das lässt sich nicht öffnen."],
            "schließe": ["Da gibt es nichts zu schließen."],
            "benutze": ["Das ist keine gute Idee.", "Das lässt sich nicht benutzen.", "Da lässt sich nichts tun."],
            "benutze mit": ["Das ist keine gute Idee", "Das lässt sich nicht benutzen.",
                            "Da lässt sich nichts tun", "Ich will das nicht tun."],
            "rede mit": ["Hallo", "Na, du auch hier?", "Ich habe keine Zeit für sowas.",
                         "Er antwortet nicht.", "Bonjour", "Salut"],
            "ziehe": ["Da bewegt sich nichts."],
            "drücke": ["Da bewegt sich nichts."],
            "betrachte": ["Nichts von interesse.", "Hübsch",
                          "Faszinierend, da ist etwas eingraviert... 'hier gibt es nichts zu sehen' Na sowas!"],
            "gebe": ["Das gebe ich doch nicht her!", "Das behalte ich lieber für mich selbst."],
            "nimm": ["Kein Interesse", "Was soll ich denn damit?", "Ich will das nicht mit mir rumschleppen."]
        }

        self.failure_phrases = [
            "Nein danke",
            "Merde, non!",
            "Das muss nicht sein.",
            "Bitte WAS soll ich tun?!",
            "Igitt, nein. Das ist etwas für die Bourgeoisie.",
            "Vergiss es!",
            "Das wird nichts",
            "Das kann jia gar nicht funktionieren"
        ]

        self.scenes = [Turm(), Giebel(), Stube(), Vorratsraum(), Flur(), Küche(), Draussen()]
        self.actual_scene = self.scenes[0]
        self.raw_input = False
        self.print(
"""
 Metz, 30. Dezember 1875. Die Stadt hat sich wieder von der Belagerung der französischen Truppen im Deutsch-Französischen 
 Krieg erholt und floriert unter den preußischen Besatzern. Auch wenn sie die Straßen als neue Polizeibeamte sicherer 
 machen, ist ihr hartes Durchgreifen von der Metzer Bevölkerung gefürchtet und gehasst.
 
 Dies ist die Geschichte von Becky, die älteste von vier Kindern eines, im Krieg reich gewordenen Vaters, der, 
 vor Furcht vor den Stadtbewohnen am Stadtrand ein altes Herrenhaus bezog. Seinen Kindern ist der Ausgang
 gänzlich verwehrt und wer nicht gehorcht, erfährt drakonische Strafen. Nicht selten kommt er des Nachts von der Taverne 
 in das Gemach von Becky, im Turm des Anwesens, um ihr mit dem Riemengürtel seine Gefühle mitzuteilen.
 Gestern war eben solch ein Tag.
 
 Du erwachst aus einem unruhigen Schlaf und findest dich in deinem Bett im Turm eures Anwesens wieder.
 Du hast auf dem Bauch geschlafen, da die Wunden an deinem Rücken noch nicht geschlossen waren letzte Nacht.
 Das Betttuch, aus dem du dich windest ist mit eingetrockneten und frischen Blutspritzern übersäht.
 Mühsam richtest du dich auf. Du erinnerst dich einen Traum, den du letzte Nacht hattest:
 Du siehst deinen Vater, wie er einen Karren durch einen nebeligen Wald fährt. Auf dem Karren liegen
 die toten Körper deiner jüngeren Geschwister Joss, Claire und Pierre. Mit einer Muskete bewaffnet stehst du vor ihm
 und versperrst ihm den Weg. Als er anhält, fängt er inbrünstig an zu lachen. Das Lachen lässt dich zu Stein erstarren,
 als er auf dich zugeht und dir das Gewehr abnimmt. Er setzt es an deine Brust und drückt ab. Ein ohrenbetäubender
 Knall erschallt im Dickicht, während dir schwarz vor Augen wird. In deinem Geiste siehst du den schimmernden 
 Mond am Himmelszelt, wie er in seiner Mächtigkeit über deinem Haupt erwacht. Er ist dein Freund. Dein Partner.
 
 Du fasst den Beschluss von hier zu fliehen, aus den Fängen deines Vaters zu entkommen.
"""
        )
        self.print(self.actual_scene.entrance)
        self.bag = []

    # custom print and input methods
    @staticmethod
    def print(string):
        print("\n", string, "\n")

    @staticmethod
    def input():
        # res = input(colors.fg.lightgrey + "> ").lower()
        print(colors.fg.lightgrey, end="")
        try:
            res = input("> ").lower()
        except:
            return GameEngine.input()
        print(colors.fg.basecolor, end="")
        return res

    def print_success(self):
        self.print(choice(self.sccuess_phrases))

    def print_failure(self, command=""):
        if command in self.failure_dic:
            self.print(choice(self.failure_dic[command]))
        else:
            self.print(choice(self.failure_phrases))

    # check if object is placed in the bag
    def in_bag(self, object_name):
        for o in self.bag:
            if o.name == object_name:
                return True
        return False

    # move to next scene
    def change_scene(self, scene_name):
        for scene in self.scenes:
            if scene.name == scene_name:
                self.actual_scene = scene
                self.print(self.actual_scene.entrance)

    # evaluate input
    def input_process(self, string):
        if not string:
            return

        # enable the handling of an raw input
        if self.raw_input:
            self.actual_scene.scene_magic(self, string)
            return

        # split input into single words
        cuts = string.lower().split()

        # check for special commands
        if len(cuts) == 1:
            if cuts[0] == self.special_commands["hilfe"]:
                self.show_help()
                return
            elif cuts[0] == self.special_commands["speichern"]:
                self.save()
                return
            elif cuts[0] == self.special_commands["beenden"]:
                self.exit()
            elif cuts[0] == self.special_commands["tasche"]:
                self.show_bag()
                return
            elif cuts[0] == self.special_commands["umsehen"]:
                self.print(self.actual_scene.entrance)
                return

            # check which command is the most matching one
        max_common_key = ""
        max_common_val = []
        max_common_number = 0
        for key, val in self.command_dic.items():
            s = val.split()
            # they have to be equal lengthed
            if len(s) != len(cuts):
                continue
            # count the matching strings
            else:
                common_number=0
                for a,b in zip(s, cuts):
                    if a == b:
                        common_number = common_number + 1
                if common_number > max_common_number:
                    max_common_number = common_number
                    max_common_key = key
                    max_common_val = val
        # no matching
        if max_common_key == "":
            self.print("Was meinst du mit '" + cuts[0] + "'?")
            return

        # turn both lists into one dict and pack the {i}'s into a new list
        lc = []
        string_dic = dict(list(zip(max_common_val.split(), cuts)))
        for i in range(0, 10):         # 10 -> maximal number of command arguments (here arbitrarily set)
            if "{" + str(i) + "}" in string_dic:
                lc.append(string_dic["{" + str(i) + "}"])
        self.actual_scene.scene_magic(self, [max_common_key, lc])

    # print important commands
    def show_help(self):
        help_str = """
        
    Befehle:
         
"""
        for key, val in self.command_dic.items():
            help_str = help_str + " " + val.format("<objekt1>", "<objekt2>") + "\n"

        help_str = help_str + """
        
    Weitere nützliche Befehle:
         
"""
        for key, val in self.special_commands.items():
            help_str = help_str + " " + val + "\n"

        self.print(help_str)

    # print bag content
    def show_bag(self):
        if len(self.bag) == 0:
            self.print("leer")
            return
        bag_str = ""
        for item in self.bag:
            bag_str = bag_str + "     " + item.name
        self.print(bag_str)

    # save game
    def save(self):
        while True:
            self.print("Spiel speichern? [j/n]")
            answer = self.input()
            if answer == "j":
                try:
                    pickle.dump(self, open("savegame.p", "wb"))
                    readline.write_history_file(".history")
                except:
                    self.print("Fehler beim Speichervorgang aufgetreten. Bitte überprüfe Schreibberechtigungen.")
                    return
                self.print("Speichern erfolgreich.")
                return
            elif answer == "n":
                return

    # exit game
    def exit(self):
        self.save()
        while True:
            self.print("Spiel beenden? [j/n]")
            answer = self.input()
            if answer == "j":
                exit()
                break
            elif answer == "n":
                break
        return










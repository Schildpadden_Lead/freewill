__author__ = "Marvin Gerlach"

class Object:

    def __init__(self, name, takeable, in_bag, command_answers, status=""):
        """
        :param name: Name of the obejct
        :param takeable: boolean, if object can be put in the bag
        :param in_bag: boolean, if object is placed inside the bag
        :param command_answers: [[command, [command_objects], [answers]]]
        :param status: status of the object for objects with multiple states
        """

        self.in_bag = in_bag
        self.name = name
        self.takeable = takeable
        self.status = status
        self.command_answers = command_answers
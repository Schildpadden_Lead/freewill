from numpy.random import choice

__author__ = "Marvin Gerlach"


class Scene:
    def __init__(self, name, object_list, entrance):
        """
        :param name: name of scene
        :param object_list: [objects]
        :param entrance: text, that tells the player what is in the scene
        """
        self.name = name
        self.objects = object_list
        self.entrance = entrance

    def scene_magic(self, game_engine, inpt):
        """
        This method serves as the logical part of the scene, if overriden
        e.g. remove objects from the scene and place them inside the bag
        This default method just answers to simple commands from object lists

        :param inpt: [command_name,[object_arguments]], e.g. ["open with",["Door", "key"]]. No check for empty values!
        :param game_engine: the reference to the game engine
        """

        # collect all command answers to given command
        commands = []
        for x in self.objects:
            for y in x.command_answers:
                if y[0].lower() == inpt[0].lower():
                    commands.append(y)
        for x in game_engine.bag:
            for y in x.command_answers:
                if y[0].lower() == inpt[0].lower():
                    commands.append(y)

        # search for matching objects and print according or failure answers
        for com in commands:
            if inpt[1][0].lower() == com[1][0].lower():
                if len(inpt[1]) == 2 and len(com[1]) == 2:
                    if inpt[1][1].lower() == com[1][1].lower():
                        game_engine.print(choice(com[2]))
                        return
                elif len(inpt[1]) == 1 and len(com[1]) == 1:
                    game_engine.print(choice(com[2]))
                    return
        game_engine.print_failure(inpt[0])
        return

    def find_object(self, object_name, bag=[]):
        """
        :returns Object by name from scene or from scene and bag, if a bag is given
        """
        for o in self.objects:
            if o.name.lower() == object_name.lower():
                return o
        for o in bag:
            if o.name.lower() == object_name.lower():
                return o
        return ""

    def both(self, o1, o2, string1, string2):
        """
        :returns true if both o1.name and o2.name equal string1 or string2
        """
        return (o1.name == string1 and o2.name == string2) or (o1.name == string2 and o2.name == string1)

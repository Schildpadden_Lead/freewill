__author__ = "Marvin Gerlach"

"""

This class is the base class for a new text adventure.

A text adventure consists of multiple scenes with objects the player can interact with via some commands.
A command has the form of a string with object placeholders {0}, {1}, ...
E.g. "open {0} with {1}" or "go to {0}"
The command dic has the command name as key and the command string as value: {"open": "open {0}", ...}

"""


class AdventureEngine:
    def __init__(self, command_dic, scenes):
        """
        :param command_dic: {command_name: command_string with object placeholders {i} }
        :param scenes: [scenes]
        """
        self.actual_scene = 0
        self.bag = []
        self.command_dic = command_dic
        self.scenes = scenes

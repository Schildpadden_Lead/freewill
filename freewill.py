#!/usr/bin/python3
import readline

__author__ = "Marvin Gerlach"

from GameEngine import GameEngine
import os
import pickle
from colors import colors, col


colors.fg.basecolor = colors.fg.lightgreen
print(colors.fg.basecolor)

# print start screen with options of starting a new game, loading an old one and exiting the program
screen_file = open("startscreen.txt", "r")
for line in screen_file:
    print(line, end="")
screen_file.close()

print("\n\n\n")
print("\t\t\t- Start (" + col("s", colors.fg.lightgrey) +
      ") -   - Laden (" + col("l", colors.fg.lightgrey) +
      ") -   - Exit (" + col("e", colors.fg.lightgrey) +
      ") -", end="\n\n")

# read and handle input
while True:
    choice = GameEngine.input()

    if choice == "e":
        exit()
    elif choice == "l":
        if os.path.exists("savegame.p"):
            os.system("clear")
            engine = pickle.load(open("savegame.p", "rb"))
            # load history file
            if os.path.exists(".history"):
                readline.read_history_file(".history")
            engine.print(engine.actual_scene.entrance)
            break
        else:
            GameEngine.print("No savegame found")
    elif choice == "s":
        os.system("clear")
        engine = GameEngine()
        break

while True:
    engine.input_process(GameEngine.input())
